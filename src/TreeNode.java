import java.util.ArrayList;
import java.util.List;

public class TreeNode<T>  {
     
      private T name;
      private TreeNode<T> parent;
      private List<TreeNode<T>> children;
      double weight;

    public TreeNode(T data) {
        this.name = data;
        this.children = new ArrayList<TreeNode<T>>();
        this.weight=0;
    }
    public TreeNode(TreeNode<T> data) {
        this.name = data.name;
        this.children = data.children;
        this.weight=0;
        this.parent=null;
    }

    public TreeNode<T> addChild(T child,double weight) {
        TreeNode<T> childNode = new TreeNode<T>(child);
        childNode.parent = this;
        this.children.add(childNode);
        childNode.weight=weight;
        return childNode;
    }
    public void deleteChield(TreeNode<T> chield) {
        this.children.remove(chield);
    }

    public TreeNode<T> getParent() {
    	  return parent;
    	 }
    public List<TreeNode<T>> getChield() {
  	  return  children;
  	 }
    public T tooString() {
		return name;
    	
    }
    public int sizeNode() {
    	  return  children.size();
    }
 }