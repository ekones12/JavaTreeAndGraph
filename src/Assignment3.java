import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;


public class Assignment3 {

	static boolean proc = true;
	
	public static ArrayList<String[]> reader (String args1 ,int control) {
			
	    BufferedReader br = null;
	    String line = "";
	    String cvsSplitBy = " ";
	    String cvsSplitBy2 = "-";
	   	ArrayList<String[]> dizi = new ArrayList<String[]>();
	  
	    try {
	        br = new BufferedReader(new FileReader(args1));
	        while ((line = br.readLine()) != null) {
	        	if(control==0)	dizi.add(line.split(cvsSplitBy));
	        	else	
	        		if(!("".equals(line))) {
	        			dizi.add(line.split(cvsSplitBy2));
	        		}
	     
	        }
	
	    } catch (FileNotFoundException e) {
	        e.printStackTrace();
	    } catch (IOException e) {
	        e.printStackTrace();
	    } finally {
	        if (br != null) {
	            try {
	                br.close();
	            } catch (IOException e) {
	                e.printStackTrace();
	            }
	        }
	    }
		return dizi;
	}
	
	
	public static int index(ArrayList<String[]> wordVec,String word) {
		int i,index;
		int length=wordVec.size();
		for (i=0;i<length;i++) {
			if(word.equals(wordVec.get(i)[0].replace("\"", "")))
				return i;
		}
		return -2;	
	}
	 
	 
	
	public static double cos_sim(ArrayList<String[]> wordVec,String word_pairs0,String word_pairs1) {
	
		
		int i;
		int length=wordVec.get(0).length;
		double dot=0;
		double vect=0;
		double vect2=0;
		double last=0;
		int word1index=index(wordVec,word_pairs0);
		int word2index=index(wordVec,word_pairs1);
		System.out.print(wordVec.get(word1index)[0] + "-"+wordVec.get(word2index)[0]+"-----similarty-->	");
		
		for (i=1;i<length;i++) {
			dot += (Double.parseDouble(wordVec.get(word1index)[i]) * Double.parseDouble(wordVec.get(word2index)[i]));
			vect +=Math.pow(Double.parseDouble(wordVec.get(word1index)[i]),2);
			vect2 +=Math.pow(Double.parseDouble(wordVec.get(word2index)[i]),2);
		}
		last = Math.sqrt(vect2*vect);
		last= dot/last;
		
		//System.out.println(last);
		
		//System.out.println(word1index + "-"+word2index);
		
		return last;
	}
	  
	public static double cos_distance(ArrayList<String[]> wordVec,String word_pairs0,int word_pairs1) {
	
		int i;
		int length=wordVec.get(0).length;
		double vect=0;
		double vect1=0;
		double last=0;
		
		int word1index=index(wordVec,word_pairs0);
		int word2index=word_pairs1;
		//System.out.print(wordVec.get(word1index)[0] + "-"+wordVec.get(word2index)[0]+"-----similarty-->	");
		
		for (i=1;i<length;i++) {
			
			vect +=(Double.parseDouble(wordVec.get(word1index)[i]) - Double.parseDouble(wordVec.get(word2index)[i]));
			vect1+= Math.pow(vect, 2);
		}
		last = Math.sqrt(vect1);
	
		
		return last;
	}
	public static double cos_long(ArrayList<String[]> wordVec,String word_pairs0,double duble) {
	
	int i;
	int length=wordVec.get(0).length;
	double vect=0;
	double vect1=0;
	double last=0;
	
	int word1index=index(wordVec,word_pairs0);
	
	//System.out.print(wordVec.get(word1index)[0] + "-"+wordVec.get(word2index)[0]+"-----similarty-->	");
	
	for (i=1;i<length;i++) {
		
		vect +=(Double.parseDouble(wordVec.get(word1index)[i])-duble);
		vect1+= Math.pow(vect, 2);
	}
	last = Math.sqrt(vect1);

	
	return last;
}
	public static void Operation(String argv,ArrayList<String[]> wordVec,ArrayList<String[]> word_pairs,int cluster) {
		Set<String> WordSet = new HashSet<String>();
;
		CompleteGraph<String> CompleteGraph= new CompleteGraph<String>();
		for(int i=0;i<word_pairs.size();i++) {
			WordSet.add(word_pairs.get(i)[0]);
			WordSet.add(word_pairs.get(i)[1]);	
		}
	
		CompleteGraph.addNodes(WordSet);
		Iterator<String> iter2 = WordSet.iterator();
		while(iter2.hasNext()){
			Iterator<String> iter = WordSet.iterator();
			String deger = iter2.next();
			Map<String,Double> Map = new HashMap<String, Double>();
			  while(iter.hasNext()){ 
			   String deger1 = iter.next();
			   Map.put(deger1,cos_sim(wordVec,deger,deger1));
			  }
		CompleteGraph.addEdges(deger, Map);
		}
		maxWeight(argv,wordVec,CompleteGraph,WordSet,cluster);
	}
	
	
	public static void mstSearch(TreeNode<String> mst,String deger,String key,double max) {

		for(TreeNode<String> chield : mst.getChield()) {
			
			if(chield.tooString().equals(deger)) {
			//System.out.println("Node :"+chield.tooString()+" katman : "+max);
					chield.addChild(key,max);
			}
			else 
				mstSearch(chield,deger,key,max);
		}
	}
	public static void mstPrint(PrintWriter output,TreeNode<String> mst) {
		if(!mst.tooString().equals("mst"))
			output.print(mst.tooString());
		for(TreeNode<String> chield : mst.getChield()) {
			if(!mst.tooString().equals("mst"))
				output.print(",");
				mstPrint(output,chield);
		}
	}
	
	public static double mstMinEdge(double min,TreeNode<String> mst) {
		for(TreeNode<String> chield : mst.getChield()) {
			if(min>chield.weight&&chield.weight!=0) {
				min = chield.weight;
			}
			min = mstMinEdge(min,chield);
		}
		return min;
		
	}
	public static TreeNode<String> mstCluster(double min,TreeNode<String> mst) {
		TreeNode<String> asd = new TreeNode<String>(mst.tooString());
		
		
		for(TreeNode<String> chield : mst.getChield()) {
			if(proc) {
			if(min==chield.weight&&chield.weight!=0) {
				//System.out.println("HELLO DEAR:"+chield.weight+"-->"+chield.tooString());
				chield.getParent().deleteChield(chield);
				asd = new TreeNode<String>(chield);
				proc=false;
				return asd;
				
			}
			else {
				asd=mstCluster(min,chield);
				}
			}
		}
		
		return asd;
		
		
	}
	public static void maxWeight(String argv,ArrayList<String[]> wordVec,CompleteGraph<String> CompleteGraph,Set<String> WordSet,int cluster ) {
		
		String deger="";
		
		Set<String> primSet = new HashSet<String>();
		Iterator<String> iter2 = WordSet.iterator();
		deger = iter2.next();
		primSet.add(deger);
		TreeNode<String> mst = new TreeNode<String>("mst");
		mst.addChild(deger,0);
		
		
		while(!(primSet.size()==WordSet.size())){
		
		double max	=	0;
		String keymax = "";
		
		for(String primset : primSet) {
		Map<String, Double> getAdj = CompleteGraph.getAdj(primset);
		
		
		Set<String> asd1 = getAdj.keySet();
		for(String pairs : asd1) {
			if(getAdj.get(pairs)<1&&!primSet.contains(pairs)) 
				if(max < getAdj.get(pairs)) {
					max = getAdj.get(pairs);
					deger=primset;
					}
		}
		Set<String> keySet = getAdj.keySet();
		Iterator<String> iter = keySet.iterator();
		while(iter.hasNext()){
			String inte = iter.next();
			if(getAdj.get(inte)==max) {
				keymax = inte;
				}
	
		  }	
		}
		//System.out.println("Start node :"+deger+"-->"+max+"<---Finish node :"+keymax+max);
		mstSearch(mst,deger,keymax,max);
		deger=keymax;
		primSet.add(deger);
		}
		//System.out.println("Distance :");
		List<TreeNode<String>> clusterlist= new ArrayList<TreeNode<String>>();
		double minedge=mstMinEdge(1,mst);
		double minedgearray[]=new double[cluster];
		
		
		clusterlist.add(mst);
		
		for(int i=0;i<cluster-1;i++) {
			double minn=1;
			int indexmst=0;
			for(int j =0;j<clusterlist.size();j++) {
				minedgearray[j]=mstMinEdge(1,clusterlist.get(j));
				//System.out.println(minedgearray[j]+"--"+clusterlist.get(j).getChield().size());
			}
			for(int j =0;j<clusterlist.size();j++) {
				if(minedgearray[j]<minn) {
					minn=minedgearray[j];
					indexmst=j;
					minedge=minn;
				}
			}
		clusterlist.add(new TreeNode<String>(mstCluster(minedge,clusterlist.get(indexmst))));
		proc=true;
		//System.out.println("\n");
		}
		PrintWriter output = null;
		String dosya = argv+"_k="+cluster+".txt";
		try{
		output = new PrintWriter (new FileOutputStream(dosya));
		}
		catch (FileNotFoundException hata)
		{
		System.out.println("Error not created output file");
		System.exit(0); 
		} 
		for(int i=0;i<clusterlist.size();i++) {
			output.print("Cluster "+(i+1)+" ==>");
			mstPrint(output,clusterlist.get(i));
			output.print("\n");
			
		}
		output.close();		
	}
	


	
	public static void main (String[] args ) {
		
		ArrayList<String[]> wordVec = reader(args[0],0); //wordVec.txt
		
		ArrayList<String[]> word_pairs = reader(args[1],1); //word_pairs.txt
		
		Operation(args[2],wordVec,word_pairs,Integer.parseInt(args[3]));
		
	
		
	
	
		
	}

}




