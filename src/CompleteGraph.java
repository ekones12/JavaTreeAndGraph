import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;

public class CompleteGraph<T> {

    private final Map<T, HashMap<T, Double>> graph;
    private Set<T> nodes;

    public CompleteGraph() {
        graph = new HashMap<T, HashMap<T, Double>>();
    }

    /**
    * Add the nodes needed to be part of a graph.
    * 
    * @param nodes     the set nodes to add in the graph.
    * @return           true if nodes list is set, and false if it is not.
    */
    public boolean addNodes (Set<T> vertex) {
        if (vertex != null) {
            if (vertex.size() == 0)
            	throw new NullPointerException("The size of node cannot be zero.");
            this.nodes = vertex;
            return true;
        }
        return false;
    }

    public void addEdges (T nodeId, Map<T, Double> allEdges) {

        addEdgeVerification(nodeId, allEdges);

        graph.put(nodeId, (HashMap<T, Double>) allEdges);
    }


    private void addEdgeVerification(T nodeId, Map<T, Double> allEdges) {
        if (!nodes.contains(nodeId)) 
        	throw new NoSuchElementException("The source node: " + nodeId + " does not exist.");

        // making sure that edges include nodes provided in the node Set.
        for (T node : allEdges.keySet()) {
            if (!nodes.contains(node)) 
            	throw new NoSuchElementException("The target node: " + nodeId + "  not exist.");
        }

        // make sure that all the nodes in the edge set are included in allEdges
        for (T node : nodes) {
            if (node != nodeId) {
                if (!allEdges.containsKey(nodeId)) 
                	throw new IllegalArgumentException("The input map does not contain all edges. ");
                }
            }
        }

    public Map<T, Double> getAdj (T nodeId) {
        if (!nodes.contains(nodeId)) 
        	throw new NoSuchElementException("The node " + nodeId + " does not exist.");
        if (!graph.containsKey(nodeId)) 
        	throw new IllegalStateException("The graph is not populated with " + nodeId);

        return graph.get(nodeId);
    }
    public int Size() {
    	
  		return graph.size();
    }
}